import React from 'react';
import { Card } from 'antd';
const { Meta } = Card;

export default function CardMovie({ dataMovie }) {
  const {
    tenPhim,
    trailer,
    hinhAnh,
    danhGia,
    hot,
    dangChieu,
    sapChieu,
    ngayKhoiChieu,
    moTa,
    maPhim,
  } = dataMovie;
  return (
    <div className="mr-5">
      <Card
        hoverable
        style={{
          width: 300,
        }}
        cover={
          <img
            alt={tenPhim}
            src={hinhAnh}
            style={{
              height: 400,
              objectFit: 'cover',
              objectPosition: 'center',
            }}
          />
        }>
        <Meta
          title={tenPhim}
          description={
            moTa.length > 50 ? (
              <span>{moTa.slice(0, 50) + '...'}</span>
            ) : (
              <span>{moTa}</span>
            )
          }
        />
        <button className="btn btn-success p-2 mt-3">Đặt vé</button>
      </Card>
    </div>
  );
}
