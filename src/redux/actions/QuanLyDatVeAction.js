import { movieServ } from '../../Pages/service/movieService';
import { userServ } from '../../Pages/service/userService';
import { ThongTinDatVe } from '../../_core/models/ThongTindatVe';
import { GET_PHONG_VE, DAT_VE } from '../types/QuanLyDatVeType';

export const getDSPhongVeAction = (maLichChieu) => {
  return (dispatch) => {
    movieServ
      .getDanhSachPhongVe(maLichChieu)
      .then((res) => {
        console.log(res.data.content);
        dispatch({
          type: GET_PHONG_VE,
          danhSachPhongVe: res.data.content,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  };
};
export const datVeAction = (thongTinDatVe = new ThongTinDatVe()) => {
  return (dispatch) => {
    movieServ
      .datVe(thongTinDatVe)
      .then((res) => {
        console.log(res.data.content);
        dispatch({
          type: DAT_VE,
          thongTinDatVe: res.data.content,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  };
};

// export const getDatVeAction = () => ({
//   type: DAT_VE,
//   gheDuocChon: ghe,
// });
