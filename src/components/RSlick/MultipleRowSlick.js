import React, { Component } from 'react';
import Slider from 'react-slick';
//import my module css
import styleSlick from './MultipleRowSlick.module.css';
import Film from '../Film/Film';
import CardMovie from '../../Pages/Home/ListMovie/CardMovie';
// react-slick
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import CardMovieFlip from '../../Pages/Home/ListMovie/CardMovieFlip';
import { useDispatch, useSelector } from 'react-redux';
import {
  DANG_CHIEU_ACTION,
  SAP_CHIEU_ACTION,
} from '../../redux/actions/movieAction';
import {
  SET_DANG_CHIEU,
  SET_SAP_CHIEU,
} from '../../redux/types/QuanLyMovieType';
import { Button } from 'flowbite-react';

// get data from state
// custome arrow
function SampleNextArrow(props) {
  const { className, style, onClick } = props;
  return (
    <div
      className={`${className} ${styleSlick['slick-prev']}`}
      style={{
        ...style,
        display: 'block',
        color: 'black',
      }}
      onClick={onClick}></div>
  );
}

function SamplePrevArrow(props) {
  const { className, style, onClick } = props;
  return (
    <div
      className={`${className} ${styleSlick['slick-next']}`}
      style={{ ...style, display: 'block' }}
      onClick={onClick}
    />
  );
}

export const MultipleRows = (props) => {
  const { dangChieu, sapChieu } = useSelector((state) => state.movieReducer);
  let activeClassDC = dangChieu === true ? 'activeFilm' : 'noneActiveFilm';
  let activeClassSC = sapChieu === true ? 'activeFilm' : 'noneActiveFilm';

  const dispatch = useDispatch();
  const renderFilms = () => {
    return props.arrMovie.map((item, index) => {
      return (
        <div key={index} className={`${styleSlick['width-item']}`}>
          {/* <Film dataMovie={item} /> */}
          {/* <CardMovie dataMovie={item} /> */}
          <CardMovieFlip dataMovie={item} />
        </div>
      );
    });
  };

  const settings = {
    className: 'center',
    centerMode: true,
    infinite: true,
    centerPadding: '60px',
    slidesToShow: 3,
    // autoplay: true,
    speed: 500,
    autoplaySpeed: 2000,
    cssEase: 'linear',
    pauseOnHover: true,

    rows: 1,
    slidesPerRow: 2,
    variableWidth: false,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
    // dots: true,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
          centerPadding: '0px',
          // centerMode: true,
          slidesToScroll: 1,
          infinite: true,
          // dots: true,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          centerPadding: '60px',
          centerMode: true,
          // className: 'left variable-width',
          slidesToScroll: 1,
          initialSlide: 1,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          centerPadding: '0px',
          slidesToScroll: 1,
        },
      },
    ],
  };
  return (
    <div>
      <div className="flex flex-wrap gap-2 justify-center items-center">
        <Button
          color="light"
          onClick={() => {
            dispatch(SAP_CHIEU_ACTION());
          }}>
          Sắp Chiếu
        </Button>
        <Button
          color="dark"
          onClick={() => {
            dispatch(DANG_CHIEU_ACTION());
          }}>
          Đang Chiếu
        </Button>
      </div>
      <Slider {...settings}>{renderFilms()}</Slider>
    </div>
  );
};

export default MultipleRows;
