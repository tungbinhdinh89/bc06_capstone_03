import React, { useEffect } from 'react';
import { Carousel } from 'flowbite-react';
import { useDispatch, useSelector } from 'react-redux';
import { getBannerAction } from '../../../redux/actions/CarouselAction';

export default function HomeCarousel() {
  const { arrBanner } = useSelector((state) => state.CarouselReducer);

  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getBannerAction());
  }, []);

  const renderBannerMovie = () => {
    return arrBanner.map((item, index) => {
      return <img key={index} src={item.hinhAnh} alt={'banner'} />;
    });
  };

  return (
    <div className="h-56 sm:h-64 xl:h-80 max-xl:h-100">
      <Carousel>{renderBannerMovie()}</Carousel>
    </div>
  );
}
