import { QUAN_LY_RAP } from '../types/QuanLyRapType';
import { movieServ } from '../../Pages/service/movieService';

export const getHeThongRapAction = () => {
  return (dispatch) => {
    movieServ
      .getCumRap()
      .then((res) => {
        console.log(res);

        dispatch({
          type: QUAN_LY_RAP,
          heThongRapChieu: res.data.content,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  };
};
