import React from 'react';
import { useSelector } from 'react-redux';
import Film from '../../../components/Film/Film';

export default function ListMovie() {
  const { arrMovie } = useSelector((state) => state.movieReducer);
  console.log('🚀 ~ arrMovie:', arrMovie);

  return (
    <div>
      <section className="text-gray-600 body-font">
        <div className="container px-5 py-24 mx-auto">
          <div className="flex flex-wrap -m-4">
            {arrMovie.map((item, index) => {
              return <Film dataMovie={item} key={index} />;
            })}
          </div>
        </div>
      </section>
    </div>
  );
}
