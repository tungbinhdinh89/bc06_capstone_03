import React from 'react';
import { Desktop, Mobile } from '../Resposive/Rersponsive';
import HeaderDesktop from './HeaderDesktop';
import HeaderMobile from './HeaderMobile';

export default function Header() {
  return (
    <div>
      <Desktop>
        <HeaderDesktop />
      </Desktop>
      <Mobile>
        <HeaderMobile />
      </Mobile>
    </div>
  );
}
