import userReducer from './userReducer';
import { CarouselReducer } from './CarouselReducer';
import SpinnerReducer from './SpinnerReducer';
import { combineReducers } from 'redux';
import movieReducer from './movieReducer';
import QuanLyRapReducer from './QuanLyRapReducer';
import QuanLyDatVeReducer from './QuanLyDatVeReducer';
export const rootReducer = combineReducers({
  userReducer,
  CarouselReducer,
  movieReducer,
  QuanLyRapReducer,
  SpinnerReducer,
  QuanLyDatVeReducer,
});
