import React, { useEffect } from 'react';
// import { Carousel } from 'antd';
import Slider from 'react-slick';
import styleSlick from '../../../components/RSlick/MultipleRowSlick.module.css';
import { useDispatch, useSelector } from 'react-redux';
import { getBannerAction } from '../../../redux/actions/CarouselAction';
import './HomeCarpusel.css';
function SampleNextArrow(props) {
  const { className, style, onClick } = props;
  return (
    <div
      className={`${className} ${styleSlick['slick-prev']}`}
      style={{
        ...style,
        display: 'block',
        color: 'black',
      }}
      onClick={onClick}></div>
  );
}

function SamplePrevArrow(props) {
  const { className, style, onClick } = props;
  return (
    <div
      className={`${className} ${styleSlick['slick-next']}`}
      style={{ ...style, display: 'block' }}
      onClick={onClick}
    />
  );
}

export default function HomeCarousel2(props) {
  // let [banner, setBanner] = useState([]);

  const { arrBanner } = useSelector((state) => state.CarouselReducer);

  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getBannerAction());
  }, []);
  const settings = {
    dots: true,
    fade: true,
    infinite: true,
    autoplay: true,
    speed: 300,
    autoplaySpeed: 2000,
    cssEase: 'linear',
    pauseOnHover: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    height: '80%',
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          dots: false,
        },
      },
      {
        breakpoint: 600,
        settings: {
          dots: false,
        },
      },
      {
        breakpoint: 480,
        settings: {
          dots: false,
        },
      },
    ],
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
  };

  let renderBannerMovie = () => {
    return arrBanner.map((item, index) => {
      return (
        <div key={index}>
          <div
            style={{
              ...settings,
              // backgroundImage: `url(${item.hinhAnh})`,
              // objectFit: 'cover',
              // objectPosition: 'top',
            }}>
            <img
              src={item.hinhAnh}
              className="w-full max-md:h-64 max-xl:h-80 2xl:h-100"
              // style={{ objectFit: 'cover', objectPosition: 'top' }}
            />
            {/* vẫn phải dùng thẻ img để cho google index */}
          </div>
        </div>
      );
    });
  };
  return (
    <div>
      <Slider {...settings}>{renderBannerMovie()}</Slider>
    </div>
  );
}
