import React, { useEffect } from 'react';
import { Carousel } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { getBannerAction } from '../../../redux/actions/CarouselAction';

export default function HomeCarousel2(props) {
  // let [banner, setBanner] = useState([]);

  const { arrBanner } = useSelector((state) => state.CarouselReducer);

  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getBannerAction());
  }, []);

  let renderBannerMovie = () => {
    const contentStyle = {
      height: '600px',
      color: '#fff',
      lineHeight: '160px',
      textAlign: 'center',
      background: '#364d79',
      backgroundPosition: 'center',
      backgroundSize: 'cover',
      backgroundRepeat: 'no-repeat',
    };
    return arrBanner.map((item, index) => {
      return (
        <div key={index}>
          <div>
            <div
              style={{
                ...contentStyle,
                // backgroundImage: `url(${item.hinhAnh})`,
                // objectFit: 'cover',
                // objectPosition: 'top',
              }}>
              <img
                src={item.hinhAnh}
                className="w-full h-full"
                // style={{ objectFit: 'cover', objectPosition: 'top' }}
              />
              {/* vẫn phải dùng thẻ img để cho google index */}
            </div>
          </div>
        </div>
      );
    });
  };
  return <Carousel effect="fade">{renderBannerMovie()}</Carousel>;
}
