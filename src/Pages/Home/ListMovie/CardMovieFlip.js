import React from 'react';
import { NavLink } from 'react-router-dom';
import './CardMovieFlip.css';

export default function CardMovieFlip({ dataMovie }) {
  const {
    tenPhim,
    trailer,
    hinhAnh,
    danhGia,
    hot,
    dangChieu,
    sapChieu,
    ngayKhoiChieu,
    moTa,
    maPhim,
  } = dataMovie;
  return (
    <div className="flip-card">
      <div className="flip-card-inner">
        <div className="flip-card-front">
          <img
            src={hinhAnh}
            alt={tenPhim}
            style={{
              width: 300,
              height: 400,
              objectFit: 'cover',
              objectPosition: 'center',
            }}
          />
        </div>
        <div className="flip-card-back">
          <h1>{tenPhim}</h1>
          <NavLink to={`/detail/${maPhim}`}>
            <button className="w-full btn btn-success">Booking</button>
          </NavLink>
        </div>
      </div>
    </div>
  );
}
