import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { HomeMenu } from './HomeMenu/HomeMenu';
import { getDataMovieAction } from '../../redux/actions/movieAction';

import MultipleRows from '../../components/RSlick/MultipleRowSlick';
import { getHeThongRapAction } from '../../redux/actions/QuanLyRapAction';
import {
  offSpinnerAction,
  onSpinnerAction,
} from '../../redux/actions/SpinnerAction';
import HomeCarousel2 from '../../templates/Layout/HomeCarousel/HomeCarousel2';
export default function Home() {
  const { arrMovie } = useSelector((state) => state.movieReducer);
  const { heThongRapChieu } = useSelector((state) => state.QuanLyRapReducer);
  const { isLoading } = useSelector((state) => state.SpinnerReducer);
  const dispatch = useDispatch();

  useEffect(() => {
    let handleOnLoading = () => {
      dispatch(onSpinnerAction());
    };
    let handleOffLoafing = () => {
      dispatch(offSpinnerAction());
    };
    dispatch(getDataMovieAction(handleOnLoading, handleOffLoafing));
    dispatch(getHeThongRapAction());
  }, []);

  return (
    <div>
      <HomeCarousel2 />
      <section className="text-gray-600 body-font">
        <div className="px-5 py-24 w-11/12 mx-auto">
          <MultipleRows arrMovie={arrMovie} />
        </div>
      </section>
      <div className="max-lg:hidden w-10/12  mx-auto">
        <HomeMenu heThongRapChieu={heThongRapChieu} />
      </div>
    </div>
  );
}
