import { QUAN_LY_RAP } from '../types/QuanLyRapType';

const initialState = {
  heThongRapChieu: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case QUAN_LY_RAP:
      state.heThongRapChieu = action.heThongRapChieu;
      return { ...state };

    default:
      return state;
  }
};
