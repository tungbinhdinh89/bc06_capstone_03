import React from 'react';
import { useSelector } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { localUserServ } from '../../../Pages/service/localService';

import { Navbar, Dropdown, Avatar, Button } from 'flowbite-react';

export default function HeaderDesktop(props) {
  let userInfor = useSelector((state) => {
    return state.userReducer.userInfor;
  });

  let handleLogout = () => {
    localUserServ.remove();
    // window.location.href = "/login";
    window.location.reload();
  };

  let renderContent = () => {
    if (userInfor) {
      return (
        <>
          <div className="items-center flex-shrink-0 hidden lg:flex space-x-5">
            <span>{userInfor.hoTen}</span>
            <span>
              {' '}
              <Avatar
                alt="User settings"
                img="https://flowbite.com/docs/images/people/profile-picture-5.jpg"
                rounded={true}
              />
            </span>

            <Button color="gray" onClick={handleLogout}>
              Đăng xuất
            </Button>
          </div>
        </>
        // <Dropdown
        //   arrowIcon={false}
        //   inline={true}
        //   label={

        //   }>
        //   <Dropdown.Item>
        //     <span className="block text-sm">Ho Ten: {userInfor.hoTen}</span>
        //   </Dropdown.Item>
        //   <Dropdown.Item>
        //     <span className="block text-sm">
        //       Tai Khoan: {userInfor.taiKhoan}
        //     </span>
        //   </Dropdown.Item>
        //   <Dropdown.Item>
        //     <span className="block text-sm">Email: {userInfor.email}</span>
        //   </Dropdown.Item>
        //   <Dropdown.Item>
        //     <span className="block text-sm">So DT: {userInfor.soDT}</span>
        //   </Dropdown.Item>

        //   <Dropdown.Divider />
        //   <Dropdown.Item>

        //   </Dropdown.Item>
        // </Dropdown>
      );
    } else {
      return (
        <>
          <div className="items-center flex justify-center pt-2 space-x-4">
            <NavLink to="/login">
              <Button color="light">Đăng Nhập</Button>
            </NavLink>
            <NavLink to="/register">
              <Button color="dark">Đăng Ký</Button>
            </NavLink>
          </div>
        </>
      );
    }
  };

  return (
    <header className="p-4 dark:bg-gray-800 dark:text-gray-100 bg-opacity-50 bg-black text-white fixed w-full z-10">
      <div className="container flex justify-between h-16 mx-auto">
        <NavLink to="/" className="hover:no-underline">
          <div className="cursor-pointer flex items-center text-4xl font-extrabold text-red-600 duration-150 animate-bounce">
            <div>
              <svg
                className="w-10 text-red-600"
                xmlns="http://www.w3.org/2000/svg"
                xmlnsXlink="http://www.w3.org/1999/xlink"
                version="1.1"
                id="Layer_1"
                x="0px"
                y="0px"
                viewBox="0 0 225 225"
                style={{ enableBackground: 'new 0 0 225 225' }}
                xmlSpace="preserve">
                <style
                  type="text/css"
                  dangerouslySetInnerHTML={{
                    __html:
                      '\n                                    .st0{fill:none;stroke:currentColor;stroke-width:20;stroke-linecap:round;stroke-miterlimit:3;}\n                                ',
                  }}
                />
                <g transform="matrix( 1, 0, 0, 1, 0,0) ">
                  <g>
                    <path
                      id="Layer0_0_1_STROKES"
                      className="st0"
                      d="M173.8,151.5l13.6-13.6 M35.4,89.9l29.1-29 M89.4,34.9v1 M137.4,187.9l-0.6-0.4     M36.6,138.7l0.2-0.2 M56.1,169.1l27.7-27.6 M63.8,111.5l74.3-74.4 M87.1,188.1L187.6,87.6 M110.8,114.5l57.8-57.8"
                    />
                  </g>
                </g>
              </svg>
            </div>
            <div
              className="flex items-center p-2
            text-4xl font-extrabold text-red-600  ">
              SonyFlix
            </div>
          </div>
        </NavLink>

        <ul className="items-stretch hidden space-x-3 lg:flex text-xl font-bold">
          <li className="flex">
            <NavLink
              to="/"
              className="flex items-center px-4 -mb-0.5   dark:border-transparent dark:text-violet-600 dark:border-violet-600 text-white hover:no-underline hover:visited">
              Lich Chieu
            </NavLink>
          </li>

          <li className="flex">
            <NavLink
              to="/#"
              className="flex items-center px-4 -mb-0.5  dark:border-transparent text-white hover:no-underline">
              Tin Tuc
            </NavLink>
          </li>
        </ul>
        <div className="space-x-5">{renderContent()}</div>
      </div>
    </header>
  );
}
