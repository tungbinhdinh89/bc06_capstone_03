import { ON_SPINNER, OFF_SPINNER } from '../types/SpinnerType';

const initialState = {
  isLoading: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ON_SPINNER: {
      state.isLoading = action.isLoading;
      return { ...state };
    }

    case OFF_SPINNER: {
      state.isLoading = action.isLoading;
      return { ...state };
    }

    default:
      return state;
  }
};
