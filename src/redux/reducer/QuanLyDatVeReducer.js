import { DAT_VE, GET_PHONG_VE } from '../types/QuanLyDatVeType';
import { ThongTinPhongVe } from '../../_core/models/ThongTinPhongVe';
import { ThongTinDatVe } from '../../_core/models/ThongTindatVe';

const initialState = {
  danhSachPhongVe: new ThongTinPhongVe(),
  danhSachGheDangDat: [],
  thongTinDatVe: new ThongTinDatVe(),
};

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_PHONG_VE:
      state.danhSachPhongVe = action.danhSachPhongVe;
      return { ...state };

    case DAT_VE: {
      console.log(action.gheDuocChon);
      // Cập nhật danh sách ghế đang đặt
      let danhSachGheCapNhat = [...state.danhSachGheDangDat];
      let index = danhSachGheCapNhat.findIndex(
        (gheDD) => gheDD.maGhe === action.gheDuocChon.maGhe
      );
      if (index !== -1) {
        // state.danhSachGheDangDat = danhSachGheCapNhat;
        // Nếu tìm thấy ghế được chọn trong mảng có nghĩa là trước đó đã click vào rồi => xoá đi
        danhSachGheCapNhat.splice(index, 1);
      } else {
        danhSachGheCapNhat.push(action.gheDuocChon);
      }
      state.thongTinDatVe = action.thongTinDatVe;
      return { ...state, danhSachGheDangDat: danhSachGheCapNhat };
    }

    default:
      return state;
  }
};
