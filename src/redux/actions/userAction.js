import { USER_LOGIN } from '../constant/userConstant';
import { userServ } from '../../Pages/service/userService';
import { localUserServ } from '../../Pages/service/localService';

export const userLoginAction = (formLogin, onSuccess, onError) => {
  return (dispatch) => {
    userServ
      .postLogin(formLogin)
      .then((res) => {
        console.log(res.data.content);
        // dispatch({
        //   type: USER_LOGIN,
        //   userInfor: res.data.content,
        // });
        // localUserServ.set(res.data.content);
        // onSuccess();
      })
      .catch((err) => {
        console.log(err);
        onError();
      });
  };
};
