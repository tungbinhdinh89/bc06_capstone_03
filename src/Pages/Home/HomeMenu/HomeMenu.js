import React, { Fragment } from 'react';
import { Tabs } from 'antd';
import TabsFilm from './TabsFilm';

// const { TabPane } = Tabs;

export const HomeMenu = React.memo(function HomeMenu({ heThongRapChieu }) {
  const renderCumRap = () => {
    return heThongRapChieu.map((rapChieu, index) => {
      return {
        key: index,
        label: (
          <Fragment>
            <img
              className="rouded-full"
              width="50"
              src={rapChieu.logo}
              alt={rapChieu.tenHeThongRap}
            />
            <hr className="mt-2" />
          </Fragment>
        ),
        children: (
          <Tabs
            style={{ height: 700 }}
            tabPosition="left"
            defaultActiveKey="1"
            items={rapChieu.lstCumRap.map((cumRap, index) => {
              return {
                key: index,
                label: (
                  <Fragment>
                    <div className="flex" style={{ width: '300px' }}>
                      <img
                        src={cumRap.hinhAnh}
                        style={{ height: '80px', height: '100px' }}
                      />
                      <div className="">
                        <p className="text-left ml-2">
                          {cumRap.tenCumRap.length > 25 ? (
                            <span>{cumRap.tenCumRap.slice(0, 25) + '...'}</span>
                          ) : (
                            <span>{cumRap.tenCumRap}</span>
                          )}
                        </p>
                        <br />
                        <p className="text-left ml-2">
                          {cumRap.diaChi.length > 25 ? (
                            <span>{cumRap.diaChi.slice(0, 25) + '...'}</span>
                          ) : (
                            <span>{cumRap.diaChi}</span>
                          )}
                        </p>
                        <br />
                        <p className="text-left ml-2 text-red-500">
                          [Chi Tiết]
                        </p>
                      </div>
                    </div>
                    <hr className="mt-2" />
                  </Fragment>
                ),
                children: (
                  <div
                    style={{
                      height: 700,
                      overflowY: 'scroll',
                    }}
                    className="space-y-2 w-full">
                    {cumRap.danhSachPhim.map((phim, index) => {
                      return <TabsFilm phim={phim} key={index} />;
                    })}
                  </div>
                ),
              };
            })}
          />
        ),
      };
    });
  };
  return (
    <>
      <Tabs
        className="border-2"
        style={{ height: 700 }}
        tabPosition="left"
        defaultActiveKey="1"
        items={renderCumRap()}
      />
    </>
  );
});
