import { GET_LICH_CHIEU, GET_MOVIE } from '../constant/movieConstant';
import { SET_DANG_CHIEU, SET_SAP_CHIEU } from '../types/QuanLyMovieType';

const initialState = {
  arrMovie: [
    {
      maPhim: 5864,
      tenPhim: 'Nhà Kho Quỷ Ám - The Shed',
      biDanh: 'nha-kho-quy-am-the-shed',
      trailer: 'https://youtu.be/I_zJHC9tteQ',
      hinhAnh:
        'https://movienew.cybersoft.edu.vn/hinhanh/nha-kho-quy-am-the-shed_gp00.jpg',
      moTa: 'Cậu thiếu niên Stan 17 tuổi vô tình phát hiện ra một ma cà rồng khát máu, vì sợ ánh mặt trời mà trốn trong nhà kho. Vốn chịu nhiều tổn thương và là kẻ yếu thế ở, Stan và cậu bạn thân quyết định dùng thứ vũ khí này để bảo vệ mình và trả thù những kẻ đã bắt nạt mình tại trường học. ‘Nhà kho quỷ ám’ của cậu trở thành mồ chôn của không biết bao nhiêu nạn nhân. Đến khi sự việc vượt ngoài tầm kiểm soát thì đã quá muộn.',
      maNhom: 'GP00',
      ngayKhoiChieu: '2022-11-20T17:16:01.807',
      danhGia: 5,
      hot: true,
      dangChieu: false,
      sapChieu: true,
    },
  ],
  dangChieu: true,
  sapChieu: true,
  arrMovieDefault: [],
  filmDetail: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_MOVIE:
      state.arrMovie = action.arrMovie;
      state.arrMovieDefault = state.arrMovie;
      return { ...state };

    case GET_LICH_CHIEU:
      state.filmDetail = action.filmDetail;

      return { ...state };

    case SET_DANG_CHIEU:
      state.dangChieu = !state.dangChieu;

      state.arrMovie = state.arrMovieDefault.filter(
        (film) => film.dangChieu === state.dangChieu
      );
      return { ...state };

    case SET_SAP_CHIEU:
      state.sapChieu = !state.sapChieu;

      state.arrMovie = state.arrMovieDefault.filter(
        (film) => film.sapChieu === state.sapChieu
      );
      return { ...state };

    default:
      return state;
  }
};
