import React, { useEffect } from 'react';
import { useFormik } from 'formik';
import { NavLink, useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { userLoginAction } from '../../redux/actions/userAction';
import { message } from 'antd';

export default function Login() {
  // const userInfor = useSelector((state) => state.userReducer.userInfor);

  const dispatch = useDispatch();
  const navigate = useNavigate();

  const formik = useFormik({
    initialValues: {
      taiKhoan: '',
      matKhau: '',
    },
    onSubmit: (values) => {
      let handleSuccess = () => {
        message.success('Đăng nhập thàngh công');
        //chuyển hướng về trang trước đó
        navigate(-1);
      };
      let handleFailed = () => {
        message.error('Đăng nhập thất bại');
      };
      dispatch(userLoginAction(values, handleSuccess, handleFailed));
    },
  });

  return (
    <form
      // mặt định khi bấm nút đăng nhập sẽ reload lại browser nếu muốn ngăn browser reload thì dùng: onSubmit = {(event)=>{event.preventDefault(); formik.handleSubmit(event)}}
      onSubmit={formik.handleSubmit}
      className="lg:w-1/2 xl:max-w-screen-sm">
      <div className="py-12 bg-red-100 lg:bg-white flex justify-center lg:justify-start lg:px-12">
        <NavLink className="hover:no-underline" to="/home">
          <div className="cursor-pointer flex items-center">
            <div>
              <svg
                className="w-10 text-red-500"
                xmlns="http://www.w3.org/2000/svg"
                xmlnsXlink="http://www.w3.org/1999/xlink"
                version="1.1"
                id="Layer_1"
                x="0px"
                y="0px"
                viewBox="0 0 225 225"
                style={{ enableBackground: 'new 0 0 225 225' }}
                xmlSpace="preserve">
                <style
                  type="text/css"
                  dangerouslySetInnerHTML={{
                    __html:
                      '\n                                    .st0{fill:none;stroke:currentColor;stroke-width:20;stroke-linecap:round;stroke-miterlimit:3;}\n                                ',
                  }}
                />
                <g transform="matrix( 1, 0, 0, 1, 0,0) ">
                  <g>
                    <path
                      id="Layer0_0_1_STROKES"
                      className="st0"
                      d="M173.8,151.5l13.6-13.6 M35.4,89.9l29.1-29 M89.4,34.9v1 M137.4,187.9l-0.6-0.4     M36.6,138.7l0.2-0.2 M56.1,169.1l27.7-27.6 M63.8,111.5l74.3-74.4 M87.1,188.1L187.6,87.6 M110.8,114.5l57.8-57.8"
                    />
                  </g>
                </g>
              </svg>
            </div>
            <div className="text-4xl text-red-500 hover:tracking-wider  ml-2 font-semibold">
              SonyFlix
            </div>
          </div>
        </NavLink>
      </div>
      <div className="mt-10 px-12 sm:px-24 md:px-48 lg:px-12 lg:mt-16 xl:px-24 xl:max-w-2xl">
        <h2
          className="text-center text-4xl text-red-900 font-display font-semibold lg:text-left xl:text-5xl
              xl:text-bold">
          Đăng Nhập
        </h2>
        <div className="mt-12">
          <div>
            <div>
              <div className=" text-left text-sm font-bold text-gray-700 tracking-wide">
                Tài Khoản
              </div>
              <input
                name="taiKhoan"
                onChange={formik.handleChange}
                className="w-full text-lg py-2 border-b border-gray-300 focus:outline-none focus:border-indigo-500"
                type="text"
                placeholder="Nhập tài khoản của bạn"
              />
            </div>
            <div className="mt-8">
              <div className="flex justify-between items-center">
                <div className="text-sm font-bold text-gray-700 tracking-wide">
                  Mật khẩu
                </div>
                <div>
                  <a
                    className="text-xs font-display font-semibold text-indigo-600 hover:text-indigo-800
                                  cursor-pointer">
                    Quên mật khẩu?
                  </a>
                </div>
              </div>
              <input
                name="matKhau"
                onChange={formik.handleChange}
                className="w-full text-lg py-2 border-b border-gray-300 focus:outline-none focus:border-indigo-500"
                type="password"
                placeholder="Nhập mật khẩu của bạn"
              />
            </div>
            <div className="mt-10">
              <button
                className="bg-red-500 text-gray-100 p-4 w-full rounded-full tracking-wide
                          font-semibold font-display focus:outline-none focus:shadow-outline hover:bg-red-600
                          shadow-lg">
                Đăng Nhập
              </button>
            </div>
          </div>
          <div className="mt-12 text-sm font-display font-semibold text-gray-700 text-center">
            Bạn chưa có tài khoản ?
            <NavLink
              className="hover:no-underline"
              style={{ texDecoration: 'none' }}
              to="/register">
              <span className=" cursor-pointer text-red-600 hover:text-red-800">
                {` Đăng ký`}
              </span>
            </NavLink>
          </div>
        </div>
      </div>
    </form>
  );
}
