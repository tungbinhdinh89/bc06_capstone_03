import React from 'react';
import { Fragment } from 'react';
import { useSelector } from 'react-redux';

export default function CheckoutTemplate({ Component }) {
  const userInfor = useSelector((state) => state.userReducer.userInfor);

  if (!userInfor) {
    return (window.location.href = '/login');
  }

  return (
    <Fragment>
      <Component />
    </Fragment>
  );
}

// export default CheckoutTemplate;
