import React from 'react';
import { useSelector } from 'react-redux';
import { HashLoader } from 'react-spinners';

export function Spinner() {
  let isLoading = useSelector((state) => {
    return state.SpinnerReducer.isLoading;
  });

  return isLoading === true ? (
    <div className="h-screen w-screen opacity-60 bg-black fixed top-0 left-0 z-10 flex justify-center items-center">
      <HashLoader color="#d64e36" size={100} />
      {/* <ClockLoader color="#36d7b7" /> */}
    </div>
  ) : (
    <></>
  );
}
