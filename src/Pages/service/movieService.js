import axios from 'axios';
import {
  BASE_URL,
  configHeader,
  GROUPID,
  tokenUser,
} from '../../util/settings/config';
import { ThongTinDatVe } from '../../_core/models/ThongTindatVe';
import { localUserServ } from '../../Pages/service/localService';

export const movieServ = {
  getDataMovie: () => {
    return axios.get(
      `${BASE_URL}/api/QuanLyPhim/LayDanhSachPhim?maNhom=${GROUPID}`,
      {
        headers: configHeader(),
      }
    );
  },
  getBannerMovie: () => {
    return axios.get(`${BASE_URL}/api/QuanLyPhim/LayDanhSachBanner`, {
      headers: configHeader(),
    });
  },
  getCumRap: () => {
    return axios.get(
      `${BASE_URL}/api/QuanLyRap/LayThongTinLichChieuHeThongRap?maNhom=${GROUPID}`,
      {
        headers: configHeader(),
      }
    );
  },
  getLichChieu: (maPhim) => {
    return axios.get(
      `${BASE_URL}/api/QuanLyRap/LayThongTinLichChieuPhim?MaPhim=${maPhim}`,
      {
        headers: configHeader(),
      }
    );
  },
  getDanhSachPhongVe: (maLichChieu) => {
    return axios.get(
      `${BASE_URL}/api/QuanLyDatVe/LayDanhSachPhongVe?MaLichChieu=${maLichChieu}`,
      {
        headers: configHeader(),
      }
    );
  },
  datVe: (thongTinDatVe = new ThongTinDatVe()) => {
    // return axios.post(
    //   `${BASE_URL}/api/QuanLyDatVe/DatVe`,
    //   { data: thongTinDatVe },
    //   {
    //     headers: configHeader(),
    //   }
    // );

    return axios({
      url: `${BASE_URL}/api/QuanLyDatVe/DatVe`,
      method: 'POST',
      data: thongTinDatVe,
      headers: (configHeader(), tokenUser()),
    });
  },
};
