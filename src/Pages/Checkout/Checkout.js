import React, { Fragment, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import style from './Checkout.module.css';
import './Checkout.css';
import { CloseOutlined } from '@ant-design/icons';
import _ from 'lodash';
import {
  datVeAction,
  getDSPhongVeAction,
} from '../../redux/actions/QuanLyDatVeAction';
import { DAT_VE } from '../../redux/types/QuanLyDatVeType';
import { NavLink } from 'react-router-dom';
import { ThongTinDatVe } from '../../_core/models/ThongTindatVe';

// import moment from 'moment';

export default function Checkout() {
  let params = useParams();

  let userInfor = useSelector((state) => state.userReducer.userInfor);

  let { danhSachPhongVe, danhSachGheDangDat } = useSelector(
    (state) => state.QuanLyDatVeReducer
  );
  console.log('🚀 ~ danhSachGheDangDat:', danhSachGheDangDat);

  // lần đầu render thì chưa có dữ liệu dispatch về state nên danhSachPhongVe chưa có dữ liệu đúng cần trả về => dùng optinal change hoặc tạo một model các thuộc tính đúng và gán lại trên state default cho phù hợp dữ liệu
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getDSPhongVeAction(params.id));
  }, []);

  const { thongTinPhim, danhSachGhe } = danhSachPhongVe;

  let { giaVe, loaiGhe, maRap, stt, tenGhe, maGhe } = danhSachGhe;

  let {
    diaChi,
    gioChieu,
    hinhAnh,
    maLichChieu,
    ngayChieu,
    tenCumRap,
    tenPhim,
    tenRap,
  } = thongTinPhim;
  const renderDanhSachGhe = () => {
    return danhSachGhe.map((ghe, index) => {
      let classGheVip = ghe.loaiGhe === 'Vip' ? 'gheVip' : 'ghe';
      let classGheDaDat = ghe.daDat === true ? 'gheDaDat' : '';
      let classGheDangDat = '';

      // Kiểm tra từng ghế render xem có trong mảng ghế đang đặt hay không
      let indexGheDD = danhSachGheDangDat.findIndex(
        (gheDD) => gheDD.maGhe === ghe.maGhe
      );
      if (indexGheDD !== -1) {
        classGheDaDat = 'gheDangDat';
      }
      return (
        <Fragment key={index}>
          <button
            onClick={() => {
              dispatch({ type: DAT_VE, gheDuocChon: ghe });
            }}
            disabled={ghe.daDat}
            className={`ghe ${classGheVip} ${classGheDangDat} ${classGheDaDat} text-center`}>
            {ghe.daDat ? (
              <CloseOutlined style={{ fontWeight: 'bold' }} />
            ) : (
              ghe.stt
            )}
          </button>

          {(index + 1) % 15 === 0 ? <br /> : ''}
        </Fragment>
      );
    });
  };

  let renderThongTinPhim = () => {
    return (
      <div className=" min-h-screen mt-2">
        <div className="grid grid-cols-12">
          <div className="col-span-9 ">
            <div className="flex flex-col items-center mt-5">
              <div
                className="bg-black opacity-90"
                style={{ width: '80%', height: '15px' }}>
                <img
                  src="https://movie-booking-project.vercel.app/img/bookticket/screen.png"
                  alt="man hinh"
                />
              </div>
              {/* <div className={`${style[`hinh-thang`]} text-center`}>
                <h3 className="text-black text-xl mt-3 z-10">Màn hình</h3>
              </div> */}
            </div>
            <div className="flex justify-center items-center">
              <div style={{ width: '80%' }} className=" text-left mt-5">
                {renderDanhSachGhe()}
              </div>
            </div>
          </div>

          <div className="col-span-3 text-left ">
            <h3 className="text-green-400 text-center text-2xl border-b-2  ">
              {`${danhSachGheDangDat
                .reduce((tongTien, ghe, index) => {
                  return (tongTien += ghe.giaVe);
                }, 0)
                .toLocaleString()} ${' VND'}`}
            </h3>
            <div className="text-left space-y-2">
              <h3 className="text-xl font-bold">{tenPhim}</h3>
              <p>
                Cụm rạp:{' '}
                <span className="text-red-600 font-bold">
                  {tenCumRap.length > 16
                    ? tenCumRap.slice(0, 16) + '...'
                    : tenCumRap}
                </span>
              </p>
              <p>
                Địa điểm:{' '}
                <span className="text-red-600 font-bold">
                  {diaChi.length > 16 ? diaChi.slice(0, 16) + '...' : diaChi}
                </span>
              </p>
              <p className="border-b-2">
                Ngày chiếu:{' '}
                <span className="text-red-600 font-bold">{ngayChieu}</span>{' '}
              </p>
              <p className="border-b-2">
                Giờ chiếu:{' '}
                <span className="text-red-600 font-bold">{gioChieu}</span>{' '}
              </p>
              <p className="border-b-2">
                Rạp: <span className="text-red-600 font-bold">{tenRap}</span>
              </p>
            </div>
            <div className="grid grid-cols-3 text-left my-2 border-b-2 justify-center items-center">
              <div className=" text-left">Chọn ghế: </div>
              {/* dùng lodash để sort: import _ from 'lodash'; */}
              {_.sortBy(danhSachGheDangDat, ['stt']).map((gheDD, index) => {
                return (
                  <span key={index} className="text-green-900 font-bold ">
                    {` Ghế ${gheDD.stt}${','}`}
                  </span>
                );
              })}
            </div>

            <div className="my-2">
              <i>Email : </i> <br />
              {userInfor.email}
            </div>
            <div className="my-2">
              <i>Phone : </i> <br />
              {userInfor.soDT}
            </div>
            <div className="  h-full flex flex-col ">
              {/* <NavLink to="/home"> */}
              <button
                className="btn btn-warning text-2xl font-bold p-2 w-full"
                onClick={() => {
                  const thongTinDatVe = new ThongTinDatVe();
                  thongTinDatVe.maLichChieu = params.id;
                  thongTinDatVe.danhSachVe = danhSachGheDangDat;
                  console.log('Tung:', thongTinDatVe);
                  dispatch(datVeAction(thongTinDatVe));
                }}>
                Đặt Vé
              </button>
              {/* </NavLink> */}
            </div>
          </div>
        </div>
      </div>
    );
  };

  return <div>{renderThongTinPhim()}</div>;
}
