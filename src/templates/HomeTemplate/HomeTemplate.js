import React, { Fragment } from 'react';
import Header from '../Layout/Header/Header';
import Footer from '../Layout/Footer/Footer';

export default function HomeTemplate({ Component }) {
  return (
    <Fragment>
      <Header />
      <Component />
      <hr className="mt-5 px-2" />
      <Footer />
    </Fragment>
  );
}
