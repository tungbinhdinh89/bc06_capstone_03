import { OFF_SPINNER, ON_SPINNER } from '../types/SpinnerType';

export const onSpinnerAction = () => ({
  type: ON_SPINNER,
  isLoading: true,
});
export const offSpinnerAction = (payload) => ({
  type: OFF_SPINNER,
  isLoading: false,
});
