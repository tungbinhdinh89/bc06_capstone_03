import React from 'react';

export default function Film({ dataMovie }) {
  const {
    tenPhim,
    trailer,
    hinhAnh,
    danhGia,
    hot,
    dangChieu,
    sapChieu,
    ngayKhoiChieu,
    moTa,
    maPhim,
  } = dataMovie;

  return (
    <div className="h-full mr-2 bg-gray-100 bg-opacity-75 px-8 pt-16 pb-24 rounded-lg overflow-hidden text-center relative">
      <div
        style={{
          background: `url(${hinhAnh}) center 100% no-repeat ,url(https://picsum.photos/1000/200)`,
          backgroundPosition: 'center',
          backgroundSize: 'cover',
        }}>
        <img
          src={hinhAnh}
          alt={tenPhim}
          className="opacity-0 w-full"
          style={{ height: 400 }}
        />
      </div>

      <h1 className="title-font sm:text-2xl text-xl font-medium text-gray-900 mb-3">
        {tenPhim}
      </h1>
      <p className="leading-relaxed mb-3">
        {moTa.length > 100 ? (
          <span>{moTa.slice(0, 100) + '...'}</span>
        ) : (
          <span>{moTa}</span>
        )}
      </p>
      <a className="text-indigo-500 inline-flex items-center">
        Đặt Vé
        <svg
          className="w-4 h-4 ml-2"
          viewBox="0 0 24 24"
          stroke="currentColor"
          strokeWidth={2}
          fill="none"
          strokeLinecap="round"
          strokeLinejoin="round">
          <path d="M5 12h14" />
          <path d="M12 5l7 7-7 7" />
        </svg>
      </a>
    </div>
  );
}
