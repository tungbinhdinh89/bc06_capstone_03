import React, { useEffect, Fragment } from 'react';
import { useParams } from 'react-router-dom';
import { Progress, Space } from 'antd';
import { Tabs } from 'antd';
import { Rate } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { getLichChieuAction } from '../../redux/actions/movieAction';
import { NavLink } from 'react-router-dom';
import moment from 'moment';
import {
  offSpinnerAction,
  onSpinnerAction,
} from '../../redux/actions/SpinnerAction';

export default function DetailPage() {
  const desc = ['Rất tệ', 'Tệ', 'Bình Thường', 'Hay', 'Tuyệt vời'];
  const params = useParams();
  const filmDetail = useSelector((state) => state.movieReducer.filmDetail);

  const dispatch = useDispatch();
  const { hinhAnh, tenPhim, moTa, ngayKhoiChieu, danhGia } = filmDetail;

  useEffect(() => {
    let handleOnLoading = () => {
      dispatch(onSpinnerAction());
    };
    let handleOffLoafing = () => {
      dispatch(offSpinnerAction());
    };
    dispatch(getLichChieuAction(params.id, handleOnLoading, handleOffLoafing));
  }, []);

  const renderLichChieu = () => {
    return filmDetail.heThongRapChieu?.map((htr, index) => {
      return {
        key: index,
        label: (
          <div className="p-2 border-b-2">
            <img
              style={{ width: 50, height: 50 }}
              src={htr.logo}
              alt="logo"></img>
            <p className="mt-2">{htr.tenHeThongRap}</p>
          </div>
        ),
        children: (
          <div style={{ height: 300, overflowY: 'scroll' }}>
            {htr.cumRapChieu?.map((cumRap, index) => {
              return (
                <div className="m-3 p-2 border-b-2" key={index}>
                  <div className="flex flex-row space-x-2 space-y-2 ">
                    <img
                      style={{ width: 50, height: 70 }}
                      src={cumRap.hinhAnh}
                    />
                    <div>
                      <p
                        style={{
                          fontSize: 20,
                          fontWeight: 'bold',
                          lineHeight: 1,
                        }}>
                        {cumRap.tenCumRap}
                      </p>
                      <p
                        style={{
                          fontSize: 16,
                        }}>
                        {cumRap.diaChi}
                      </p>
                    </div>
                  </div>

                  <Fragment>
                    {cumRap.lichChieuPhim?.map((lichChieu, index) => {
                      return (
                        <NavLink
                          to={`/checkout/${lichChieu.maLichChieu}`}
                          key={index}>
                          <div className="thong-tin-lich-chieu mt-2 text-left btn px-3 py-2 font-semibold border rounded dark:border-gray-100 dark:text-green-600">
                            <p className="  ">
                              {`Ngày Chiếu: ${moment(
                                lichChieu.ngayChieuGioChieu
                              ).format('DD-MM-YYYY')}`}
                            </p>
                            <p className="text-red-500 !important">
                              {`Giờ chiếu :  ${moment(
                                lichChieu.ngayChieuGioChieu
                              ).format('HH:MM A')}`}
                            </p>
                          </div>
                        </NavLink>
                      );
                    })}
                  </Fragment>
                </div>
              );
            })}
          </div>
        ),
      };
    });
  };
  const items = [
    {
      key: '1',
      label: `Lịch Chiếu`,
      children: (
        <div>
          <Tabs
            tabPosition="left"
            defaultActiveKey="1"
            items={renderLichChieu()}
          />
        </div>
      ),
    },
    {
      key: '2',
      label: `Thông Tin`,
      children: 'dasdsad',
    },
    {
      key: '3',
      label: `Đánh Giá`,
      children: `Content of Tab Pane 3`,
    },
  ];

  return (
    <div
      className="contenair bg-cover min-h-screen w-full flex justify-center items-center bg-opacity-60 backdrop-filter backdrop-blur-lg w-1/2 bg-white p-5 rounded-xl bg-opacity-60 backdrop-filter backdrop-blur-lg"
      style={{
        backgroundImage:
          'url("https://images.unsplash.com/photo-1519681393784-d120267933ba?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1124&q=100")',
      }}>
      <div className="" style={{ backgroundImage: `url()` }}>
        <div className="grid grid-cols-12 space-x-40">
          <div className="col-span-6 col-start-2 ">
            <div className="grid grid-cols-2 space-x-10">
              <img src={hinhAnh} alt="aaaa" />
              <div className="space-y-3">
                <p className="text-sm text-white">
                  Ngày khởi chiếu : {moment(ngayKhoiChieu).format(`DD/MM/YYYY`)}
                </p>
                <p className="text-4xl text-white">Tên Phim : {tenPhim}</p>
                <p className="text-white"> {moTa}</p>
                <p className="text-white">120 phút</p>
                <button className="btn btn-danger">Mua Ve</button>
              </div>
            </div>
          </div>
          <div className="col-span-4">
            <Space wrap>
              <Progress
                strokeWidth={10}
                strokeColor={{
                  '0%': 'yellow',
                  '100%': 'red',
                }}
                format={(percent) => `${percent / 10} Điểm`}
                type="circle"
                percent={danhGia * 10}
                size={200}
              />
            </Space>

            <h1 className="text-red-500 text-2xl">
              <Rate tooltips={desc} allowHalf value={danhGia / 2} />
              {danhGia / 2 ? (
                <span className="ant-rate-text">
                  {desc[Math.floor(danhGia / 2) - 1]}
                </span>
              ) : (
                ''
              )}
            </h1>
          </div>
        </div>
        <div className="mt-5 container bg-white p-3">
          <Tabs
            style={{ height: 400 }}
            defaultActiveKey="1"
            items={items}
            centered
          />
        </div>
        ;
      </div>
    </div>
  );
}
