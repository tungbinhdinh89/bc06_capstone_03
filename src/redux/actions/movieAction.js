import { GET_LICH_CHIEU, GET_MOVIE } from '../constant/movieConstant';
import { movieServ } from '../../Pages/service/movieService';
import { SET_DANG_CHIEU, SET_SAP_CHIEU } from '../types/QuanLyMovieType';

export const getDataMovieAction = (onLoading, offLongding) => {
  return (dispatch) => {
    onLoading();

    movieServ
      .getDataMovie()
      .then((res) => {
        console.log(res);
        // console.log(res.data.content);

        dispatch({
          type: GET_MOVIE,
          arrMovie: res.data.content,
        });
        offLongding();
      })
      .catch((err) => {
        console.log(err);
        offLongding();
      });
  };
};

export const getLichChieuAction = (id, onLoading, offLongding) => {
  return (dispatch) => {
    onLoading();
    movieServ
      .getLichChieu(id)
      .then((res) => {
        console.log(res);
        console.log('ting:', res.data.content);

        dispatch({
          type: GET_LICH_CHIEU,
          filmDetail: res.data.content,
        });
        offLongding();
      })
      .catch((err) => {
        console.log(err);
        offLongding();
      });
  };
};

export const DANG_CHIEU_ACTION = () => ({
  type: SET_DANG_CHIEU,
});

export const SAP_CHIEU_ACTION = () => ({
  type: SET_SAP_CHIEU,
});
