import { GET_BANNER } from '../constant/movieConstant';

const initialState = {
  arrBanner: [
    {
      maBanner: 1,
      maPhim: 1282,
      hinhAnh: 'https://movienew.cybersoft.edu.vn/hinhanh/ban-tay-diet-quy.png',
    },
  ],
};

export const CarouselReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_BANNER: {
      state.arrBanner = action.arrBanner;
      return { ...state };
    }
    default:
      return state;
  }
};
