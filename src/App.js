// import logo from './logo.svg';
import './App.css';
import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
// import LoginPage from './Pages/LoginPage/LoginPage';
// import DetailPage from './Pages/DetailPage/DetailPage';
import Home from './Pages/Home/Home';
import Contact from './Pages/Contact/Contact';
import Login from './Pages/Login/Login';
import Register from './Pages/Register/Register';
import News from './Pages/News/News';
import HomeTemplate from './templates/HomeTemplate/HomeTemplate';
import DetailPage from './Pages/DetailPage/DetailPage';
import Checkout from './Pages/Checkout/Checkout';
import { Spinner } from './components/Spinner/Spinner';
import CheckoutTemplate from './templates/CheckoutTemplate/CheckoutTemplate';
import LoginPage from './Pages/LoginPage/LoginPage';
import UserTemplate from './templates/UserTemplate/UserTemplate';

function App() {
  return (
    <div className="App">
      <Spinner />
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<HomeTemplate Component={Home} />} />
          <Route path="/home" element={<HomeTemplate Component={Home} />} />
          <Route
            path="/contact"
            element={<HomeTemplate Component={Contact} />}
          />
          <Route
            path="/detail/:id"
            element={<HomeTemplate Component={DetailPage} />}
          />
          <Route
            path="/checkout/:id"
            element={<CheckoutTemplate Component={Checkout} />}
          />
          <Route path="/login" element={<UserTemplate Component={Login} />} />
          <Route path="/register" element={<Register />} />
          <Route path="/news" element={<HomeTemplate Component={News} />} />
          {/* <Route path="/login" element={<LoginPage />} /> */}
          {/* <Route path="/detail/:id" element={<DetailPage />} /> */}
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
