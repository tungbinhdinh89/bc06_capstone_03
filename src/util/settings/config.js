export const BASE_URL = 'https://movienew.cybersoft.edu.vn';
export const GROUPID = `GP00`;
export const configHeader = () => {
  return {
    TokenCybersoft:
      'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCBTw6FuZyAwNiIsIkhldEhhblN0cmluZyI6IjExLzA5LzIwMjMiLCJIZXRIYW5UaW1lIjoiMTY5NDM5MDQwMDAwMCIsIm5iZiI6MTY3MDYwNTIwMCwiZXhwIjoxNjk0NTM4MDAwfQ.ndRbsF4IYaA6syfyNt9AVnZnm1kn9MAHUKKo4rXHLVE',
  };
};
export const tokenUser = () => {
  return {
    Authorization:
      'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoiYWJjMTIzIiwiaHR0cDovL3NjaGVtYXMueG1sc29hcC5vcmcvd3MvMjAwNS8wNS9pZGVudGl0eS9jbGFpbXMvZW1haWxhZGRyZXNzIjoiZWdnZzEyMzc5QGdtYWlsLmNvbSIsImh0dHA6Ly9zY2hlbWFzLm1pY3Jvc29mdC5jb20vd3MvMjAwOC8wNi9pZGVudGl0eS9jbGFpbXMvcm9sZSI6WyJRdWFuVHJpIiwiZWdnZzEyMzc5QGdtYWlsLmNvbSIsIkdQMDEiXSwibmJmIjoxNjgxODgwODkxLCJleHAiOjE2ODE4ODQ0OTF9.zsgHExD7k0PStIoNlrZCZkVmUdD2x2qbVZJIgzSRzfk',
  };
};
