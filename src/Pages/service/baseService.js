import Axios from 'axios';

import { DOMAIN, TOKEN } from '../../util/settings/config_old';

export class baseService {
  //put json về phía backend
  put = (url, model) => {
    return Axios({
      url: `${DOMAIN}/${url}`,
      method: 'PUT',
      data: model,
      headers: TOKEN(), //JWT
    });
  };

  post = (url, model) => {
    return Axios({
      url: `${DOMAIN}/${url}`,
      method: 'POST',
      data: model,
      headers: TOKEN(), //JWT
    });
  };

  get = (url) => {
    return Axios({
      url: `${DOMAIN}/${url}`,
      method: 'GET',
      headers: TOKEN(), //token yêu cầu từ backend chứng minh user đã đăng nhập rồi
    });
  };

  delete = (url) => {
    return Axios({
      url: `${DOMAIN}/${url}`,
      method: 'DELETE',
      headers: TOKEN(), //token yêu cầu từ backend chứng minh user đã đăng nhập rồi
    });
  };
}
