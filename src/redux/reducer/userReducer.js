import { localUserServ } from '../../Pages/service/localService';
import { USER_LOGIN } from '../constant/userConstant';
export const initialState = {
  userInfor: localUserServ.get(),
};

export default (state = initialState, action) => {
  switch (action.type) {
    case USER_LOGIN:
      state.userInfor = action.userInfor;
      return { ...state };

    default:
      return state;
  }
};
