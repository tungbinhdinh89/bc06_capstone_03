export class ThongTinPhongVe {
  thongTinPhim = new ThongTinPhim();
  danhSachGhe = new Array();
}

export class ThongTinPhim {
  diaChi = '';
  gioChieu = '';
  hinhAnh = '';
  maLichChieu = '';
  ngayChieu = '';
  tenCumRap = '';
  tenPhim = '';
  tenRap = '';
}

export class Ghe {
  maGhe = '';
  tenGhe = '';
  maRap = '';
  loaiGhe = '';
  stt = '';
  giaVe = '';
  daDat = '';
  taiKhoanNguoiDat = '';
}
