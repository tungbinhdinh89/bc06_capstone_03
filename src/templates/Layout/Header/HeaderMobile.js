import React from 'react';
import { Navbar, Dropdown, Avatar, Button } from 'flowbite-react';
import style from './header.module.css';
import { NavLink } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { localUserServ } from '../../../Pages/service/localService';

export default function HeaderMobile() {
  let userInfor = useSelector((state) => {
    return state.userReducer.userInfor;
  });
  console.log('userInfor: ', userInfor);

  let handleLogout = () => {
    localUserServ.remove();
    // window.location.href = "/login";
    window.location.reload();
  };

  let renderContent = () => {
    if (userInfor) {
      return (
        <>
          {/* <div className="items-center flex-shrink-0 hidden lg:flex space-x-5">
            <span>{userInfor.hoTen}</span>
            <Button onClick={handleLogout}>Đăng xuất</Button>
          </div> */}
          <Dropdown
            arrowIcon={false}
            inline={true}
            label={
              <Avatar
                alt="User settings"
                img="https://flowbite.com/docs/images/people/profile-picture-5.jpg"
                rounded={true}
              />
            }>
            <Dropdown.Item>
              <span className="block text-sm">Ho Ten: {userInfor.hoTen}</span>
            </Dropdown.Item>
            <Dropdown.Item>
              <span className="block text-sm">
                Tai Khoan: {userInfor.taiKhoan}
              </span>
            </Dropdown.Item>
            <Dropdown.Item>
              <span className="block text-sm">Email: {userInfor.email}</span>
            </Dropdown.Item>
            <Dropdown.Item>
              <span className="block text-sm">So DT: {userInfor.soDT}</span>
            </Dropdown.Item>
            {/* <Dropdown.Header>
              <span className="block text-sm">{userInfor.hoTen}</span>
              <span className="block truncate text-sm font-medium">
                name@flowbite.com
              </span>
            </Dropdown.Header> */}
            <Dropdown.Divider />
            <Dropdown.Item>
              <Button color="gray" onClick={handleLogout}>
                Đăng xuất
              </Button>
            </Dropdown.Item>
          </Dropdown>
        </>
      );
    } else {
      return (
        <>
          <div className="items-center flex-shrink-0 hidden lg:flex space-x-5">
            <Navbar.Link href="/login">
              <Button color="dark">Đăng Nhập</Button>
            </Navbar.Link>
            <Navbar.Link href="/register">
              <Button color="light">Đăng Ký</Button>
            </Navbar.Link>
          </div>
        </>
      );
    }
  };

  return (
    <Navbar fluid={true} rounded={true} className="hover:no-underline">
      <NavLink to="/home" className="hover:no-underline">
        <div className="cursor-pointer flex items-center text-4xl font-extrabold text-red-600 duration-150 animate-bounce">
          <div>
            <svg
              className="w-10 text-red-600"
              xmlns="http://www.w3.org/2000/svg"
              xmlnsXlink="http://www.w3.org/1999/xlink"
              version="1.1"
              id="Layer_1"
              x="0px"
              y="0px"
              viewBox="0 0 225 225"
              style={{ enableBackground: 'new 0 0 225 225' }}
              xmlSpace="preserve">
              <style
                type="text/css"
                dangerouslySetInnerHTML={{
                  __html:
                    '\n                                    .st0{fill:none;stroke:currentColor;stroke-width:20;stroke-linecap:round;stroke-miterlimit:3;}\n                                ',
                }}
              />
              <g transform="matrix( 1, 0, 0, 1, 0,0) ">
                <g>
                  <path
                    id="Layer0_0_1_STROKES"
                    className="st0"
                    d="M173.8,151.5l13.6-13.6 M35.4,89.9l29.1-29 M89.4,34.9v1 M137.4,187.9l-0.6-0.4     M36.6,138.7l0.2-0.2 M56.1,169.1l27.7-27.6 M63.8,111.5l74.3-74.4 M87.1,188.1L187.6,87.6 M110.8,114.5l57.8-57.8"
                  />
                </g>
              </g>
            </svg>
          </div>
          <div
            className="flex items-center p-2
            text-4xl font-extrabold text-red-600  ">
            SonyFlix
          </div>
        </div>
      </NavLink>
      <Navbar.Toggle />

      <Navbar.Collapse>
        <Navbar.Link href="/navbars" active={true}>
          Home
        </Navbar.Link>
        <Navbar.Link href="/navbars">About</Navbar.Link>
        <Navbar.Link href="/navbars">Services</Navbar.Link>
        <Navbar.Link href="/navbars">Pricing</Navbar.Link>
        <Navbar.Link href="/navbars">Contact</Navbar.Link>
        {userInfor == null && (
          <Navbar.Link href="/login">Đăng Nhập</Navbar.Link>
        )}
        {userInfor == null && (
          <Navbar.Link href="/register">Đăng Ký</Navbar.Link>
        )}
      </Navbar.Collapse>
      <>{renderContent()}</>
    </Navbar>
  );
}
