import React, { Fragment } from 'react';
import moment from 'moment';

export default function TabsFilm({ phim }) {
  return (
    <div className="flex items-center space-x-5 border-gray border-b pb-2">
      <img
        className="h-48 w-28 object-cover object-top rounded"
        src={phim.hinhAnh}
        alt={phim.tenPhim}
      />
      <div>
        <h5 className="font-medium text-xl mb-3">{phim.tenPhim}</h5>
        <div className="grid gap-5 grid-cols-2">
          {phim.lstLichChieuTheoPhim.slice(0, 6).map((item, index) => {
            return (
              <span
                key={index}
                className="btn px-8 py-3 font-semibold border rounded dark:border-gray-100 dark:text-green-600 ">
                {moment(item.ngayChieuGioChieu).format('DD-MM-YYYY')} ~{' '}
                <span className="text-red-500 !important">
                  {moment(item.ngayChieuGioChieu).format('HH:MM A')}
                </span>
              </span>
            );
          })}
        </div>
      </div>
    </div>
  );
}
