import React from 'react';
import { Button, Checkbox, Form, Input, message } from 'antd';
import Lottie from 'lottie-react';
import login_animated from '../../assets/login_animate.json';
import { useDispatch } from 'react-redux';
import { userLoginAction } from '../../redux/actions/userAction';

import { useNavigate } from 'react-router-dom';

export default function LoginPage() {
  let navigate = useNavigate();
  let dispatch = useDispatch();
  const onFinish = (values) => {
    let handleSuccess = (values) => {
      message.success('Login Success!');
      navigate('/');
    };
    let handleError = (values) => {
      message.error('Login Failed!');
    };

    console.log('Success:', values);
    dispatch(userLoginAction(values, handleSuccess, handleError));
  };
  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };

  return (
    <div
      className="flex w-screen h-screen justify-center items-center mt-auto"
      style={{ background: '#e76f51' }}>
      <div className="flex bg-slate-100 w-6/12 rounded p-10 justify-center items-center">
        <div className="w-1/2 h-full">
          <Form
            name="basic"
            labelCol={{
              span: 8,
            }}
            wrapperCol={{
              span: 16,
            }}
            style={{
              maxWidth: 600,
            }}
            initialValues={{
              remember: true,
            }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off">
            <Form.Item
              label="Username"
              name="taiKhoan"
              rules={[
                {
                  required: true,
                  message: 'Please input your username!',
                },
              ]}>
              <Input />
            </Form.Item>

            <Form.Item
              label="Password"
              name="matKhau"
              rules={[
                {
                  required: true,
                  message: 'Please input your password!',
                },
              ]}>
              <Input.Password />
            </Form.Item>

            <Form.Item
              name="remember"
              valuePropName="checked"
              wrapperCol={{
                offset: 8,
                span: 16,
              }}>
              <Checkbox>Remember me</Checkbox>
            </Form.Item>

            <Form.Item
              wrapperCol={{
                offset: 8,
                span: 16,
              }}>
              <Button type="primary" htmlType="submit" className="bg-red-600">
                Submit
              </Button>
            </Form.Item>
          </Form>
        </div>
        <div className="w-1/2 h-full">
          <Lottie animationData={login_animated} />
        </div>
      </div>
    </div>
  );
}
