import { GET_BANNER } from '../constant/movieConstant';
import { movieServ } from '../../Pages/service/movieService';
// import { layDanhSachBanner } from '../../Pages/service/QuanLyPhimService';

export const getBannerAction = () => {
  return (dispatch) => {
    movieServ
      .getBannerMovie()
      .then((res) => {
        console.log(res);
        // dispatch lên state
        dispatch({
          type: GET_BANNER,
          arrBanner: res.data.content,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  };
};
